//
//  ViewController.swift
//  gitlab-CI-BMi
//
//  Created by iosdev on 3.4.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var Label: UILabel!
    
    @IBOutlet weak var LabelHeight: UILabel!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    
    @IBOutlet weak var bmibutton: UIButton!
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func calculate(_ sender: UIButton) {
        //updxateButton()
        calculateBmi()
        
    }
    
    
    
    // setting the arrays
    
    
    
    var weightArray = Array<String>()
    var heightArray  = Array<String>()
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bmibutton.isEnabled = false
        
        nameTextField.delegate = self
        
        // Do any additional setup after loading the view.
        for i in 20..<175 {
            
       weightArray.append("\(i)")
            
            
    }
        pickerView.dataSource = self
        pickerView.delegate = self
        print(weightArray)
        //weightArray.remove(at: 0)
    
        for j in 90..<230 {
            heightArray.append("\(j)")
        }
        
        
        

          
        //heightArray.remove(at: 0)

}
    
    // functions for calculating number of components and etc
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            
            return weightArray.count
            
        }
        
        return heightArray.count
    }
    
     func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            
            return weightArray[row]
            
        }
        
        return heightArray[row]
        
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
            
            if component == 0   {
                Label.text = weightArray[row]
            }
            else if component == 1 {
                LabelHeight.text = heightArray[row]
                
            }
            
            
    
    }
    // Mark: function to update button
    func updxateButton(){
        let texti = nameTextField.text ?? ""
        
        bmibutton.isEnabled = !texti.isEmpty
    }
    
    // Mark: function to handle text input
    
     func textFieldDidBeginEditing(_ TextField: UITextField) {
         // Disable the Save button while editing.
         bmibutton.isEnabled = false
     }
     
     func textFieldShouldReturn(_ TextField: UITextField) -> Bool {
         //Hide keyboard
         nameTextField.resignFirstResponder()
         return true
         
     }
     func textFieldDidEndEditing(_ TextField: UITextField) {
         
         updxateButton()
        
         
     }
    
    func calculateBmi(){
        
        
        let texti = nameTextField.text
        
        let weight = Double(Label.text!) ?? 444
        let heightInit  = Double(LabelHeight.text!) ?? 0
        
        let height = heightInit*0.01
        
        let result:Double  = weight / ( height * height )
        let final = result.rounded()/10
        
        if final < 18.5 {
            
            resultLabel.text = "\(texti ?? "zak") You had \(final) : Underweight category"
            
        }   else if final > 18.5 && final <= 25 {
            
            resultLabel.text = "\(texti ?? "zak") You had \(final) : Normal weight category"
            
        }   else if final > 25 && final < 30 {
            
            resultLabel.text = "\(texti ?? "zak") You had\(final) : Overweight category"
            
        }  else  if final >= 30 {
            
            resultLabel.text = "\(texti ?? "zak") You had \(final) : obesity category"
            
        }
        
        
    
    }
    
    
}

